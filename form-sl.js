/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
 define(['N/runtime','N/ui/serverWidget'],

 function(RUNTIME, UI) {

     /**
      * Definition of the Suitelet script trigger point.
      *
      * @param {Object} context
      * @param {ServerRequest} context.request - Encapsulation of the incoming request
      * @param {ServerResponse} context.response - Encapsulation of the Suitelet response
      * @Since 2015.2
      */
    function onRequest(context) {
        var response = context.response;
        try {
            var form = UI.createForm({
                title : 'HandleBarJS in Netsuite',
                hideNavBar: true
            });
            var template = RUNTIME.getCurrentScript().getParameter("custscript2");
            var template_field = form.addField({
                id: "custpage_template",
                type: "inlinehtml",
                label: "Template"
            });
            template_field.defaultValue = template;
            
            //form.clientScriptModulePath = './';
            response.writePage(form);            
        } catch(e) {
            log.debug({
                title: "ERROR",
                details: e.toString()
            });
        }
    } 


    return {
        onRequest: onRequest
    };

 });
